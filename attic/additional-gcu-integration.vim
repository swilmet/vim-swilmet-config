"---------------------------
" Align args on parenthesis
"---------------------------

function! AlignArgsOnParenthesis()
	execute "normal :'<,'>!gcu-align-args-on-parenthesis\<CR>"
endfunction

function! MapAlignArgsOnParenthesis()
	map = :call AlignArgsOnParenthesis()<CR>
endfunction
