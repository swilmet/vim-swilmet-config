"----------------
" General config
"----------------

" Get the defaults that most users want.
source $VIMRUNTIME/defaults.vim

set background=dark
"colorscheme delek
"colorscheme torte
"colorscheme zellner
let g:solarized_termcolors=256
colorscheme solarized

" The mouse can be used
" Copy/paste with shift+clic
set mouse=a

" Set search as case insensitive, except if there is an uppercase letter.
" Maybe create a function that can easily be called when needed.
"set ignorecase
"set smartcase

" Always show a statusbar at the bottom of the screen
set laststatus=2

" Filename completion
set wildmode=longest:list

" Add dash for word completion with Ctrl+N/Ctrl+P.
" Especially useful when completing filenames in git commit messages.
set iskeyword+=-

" Repeat last command and put cursor at start of change
nmap . .`[

" Status line
" %f		file path
" %m		modified flag
" %r		read-only flag
" %y		type of file
" ff=%{&ff}	file format (unix, dos, …)
" %=		right-align following items
" Ln %l/%L	line number/total number of lines
" Col %c%V	column number and virtual column number
" %p%%		line number percentage (%l/%L*100)
"
" Use let &option="...", not the set command, to avoid lots of backslashes
" before spaces.
let &statusline="%f %m%r%y ff=%{&ff} %= Ln %l/%L  Col %c%V     %p%%"

"---------------------------
" Turn something on and off
"---------------------------

" F2: turn paste on and off
set pastetoggle=<F2>
" Get feedback in normal mode
nnoremap <F2> :set paste!<CR>:set paste?<CR>

" F3: turn search highlighting on and off
map  <F3>      :set hls!<bar>set hls?<CR>
imap <F3> <Esc>:set hls!<bar>set hls?<CR>i

"---------------
" Setup plugins
"---------------

" vim-easy-c plugin
"-------------------

" cpp is for *.h files (recognized as C++).
autocmd Filetype c,cpp,vim call EasyC_HighlightUnwantedSpaces()
autocmd BufEnter *.c,*.h nmap ,s :call EasyC_SwitchSourceHeader()<CR>
autocmd Filetype c,cpp call EasyC_SetupCompletion()
autocmd Filetype c,cpp call EasyC_SetupCodingStyle_gedit_style()

" Devhelp plugin
"----------------

let g:devhelpSearch = 1
let g:devhelpSearchKey = '<F5>'
let g:devhelpAssistant = 0

" vim-gtk-doc-comments plugin
"-----------------------------

autocmd Filetype c,cpp call MapGnomeDocImproved()

"---------------------------
" Additional customizations
"---------------------------

if filereadable(expand('~/.vim/extras.vim'))
	source ~/.vim/extras.vim
endif
