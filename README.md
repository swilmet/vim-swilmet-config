vim-swilmet-config
==================

Most of my Vim config (the useful parts for other people).

https://gitlab.gnome.org/swilmet/vim-swilmet-config

For the C language, mainly
--------------------------

I use Vim almost only for the C language, or when I need a feature that doesn't
exist in other text editors. For all other text files, I do cat-fooding of
gedit, gnome-latex, etc. (gedit is too basic for programming in C).

Related repositories
--------------------

- [vim-easy-c](https://gitlab.gnome.org/swilmet/vim-easy-c)
- [vim-gtk-doc-comments](https://gitlab.gnome.org/swilmet/vim-gtk-doc-comments)
- The Vim plugin of [Devhelp](https://gitlab.gnome.org/GNOME/devhelp)
- [gdev-c-utils](https://gitlab.gnome.org/swilmet/gdev-c-utils)

Clone and pulls
---------------

The git repository needs to be located at `~/.vim/`.

### Submodules handling

After a git clone or a git pull:

```
$ git submodule deinit --force --all
$ git submodule init
$ git submodule update
```

To add a plugin
---------------

```
git submodule add -- <url> pack/my-plugins/start/<name>
```

To remove a plugin
------------------

```
git submodule deinit -f -- pack/my-plugins/start/<name>
Remove submodule from the .gitmodules file.
rm -rf pack/my-plugins/start/<name>
Maybe also edit .git/config
Optionally do a cleanup inside .git/modules/
```

Acknowledgments
---------------

Inspired by: https://github.com/danni/vim-config (appeared on Planet GNOME if I
recall correctly).
